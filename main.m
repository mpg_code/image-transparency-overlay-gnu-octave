rgb1 = imread('peppers.png');
rgb1 = double(rgb1);
rgb1 = rgb1 ./ max(rgb1(:));
rgb2 = rgb1;

ycbcr1 = _rgb2ycbcr(rgb1);
ycbcr2 = _rgb2ycbcr(rgb2);

[M,N,O] = size(rgb1);
block_size = 50;
P = floor(M / block_size);
Q = floor(N / block_size);

for i=1:P
  for j=1:Q
    if mod(i, 2) == 0
      if mod(j, 2) == 0
        ycbcr2(1+int32((i-1)*block_size):(i*block_size),
            1+int32((j-1)*block_size):(j*block_size), 2) = 1/2;
        ycbcr2(1+int32((i-1)*block_size):(i*block_size),
            1+int32((j-1)*block_size):(j*block_size), 3) = 1/2;
      end
    end
  end
end

% use the luma from image 1 in image 2 (even though they're identical in this
% example)
ycbcr2(:,:,1) = ycbcr1(:,:,1);
rgb = _ycbcr2rgb(ycbcr2);

imshow(rgb);
print('-dpng', 'output.png');
